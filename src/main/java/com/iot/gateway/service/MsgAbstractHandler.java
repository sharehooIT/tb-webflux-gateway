package com.iot.gateway.service;

import reactor.core.publisher.Mono;

public abstract class MsgAbstractHandler<T> {

    public T protocol;

    public MsgAbstractHandler(T protocol) {
        this.protocol = protocol;
    }

    public abstract Mono<String> handle(Object msg);
}
