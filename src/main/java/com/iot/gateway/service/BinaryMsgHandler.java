package com.iot.gateway.service;

import com.iot.gateway.model.BinaryProtocol;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Slf4j
public class BinaryMsgHandler extends MsgAbstractHandler<BinaryProtocol>{

    public BinaryMsgHandler(BinaryProtocol protocol) {
        super(protocol);
    }

    @Override
    public Mono<String> handle(Object msg) {

        log.info("############:当前线程：{}, 处理消息内容：{}", Thread.currentThread().getName(), msg.toString());
        return Mono.justOrEmpty(msg.toString()).doOnNext(protocol::setName).switchIfEmpty(Mono.error(() -> new Exception("消息[]不存在")));
    }
}
