package com.iot.gateway.service;

import com.iot.gateway.model.Tweet;
import reactor.core.publisher.Mono;

public interface TweetService {

    Mono<Tweet> findOne(Long uuid);
}
