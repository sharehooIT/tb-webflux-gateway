package com.iot.gateway.service.impl;

import com.iot.gateway.model.Tweet;
import com.iot.gateway.repository.TweetRepository;
import com.iot.gateway.service.TweetService;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;

@Service
public class TweetServiceImpl implements TweetService {

    @Resource
    private TweetRepository repository;

    @Override
    public Mono<Tweet> findOne(Long uuid) {
        return repository.findById(uuid);
    }
}
