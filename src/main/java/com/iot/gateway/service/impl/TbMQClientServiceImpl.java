package com.iot.gateway.service.impl;

import com.iot.gateway.service.TbMQClientService;
import net.dreamlu.iot.mqtt.spring.client.MqttClientTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;


@Service
public class TbMQClientServiceImpl implements TbMQClientService {

    @Resource
    private MqttClientTemplate client;

    @Override
    public boolean publish(String body, String topic) {
        client.publish(topic, ByteBuffer.wrap(body.getBytes(StandardCharsets.UTF_8)));
        return false;
    }
}
