package com.iot.gateway.service;

/**
 * @desc thingsboard客户端service
 */
public interface TbMQClientService {

    boolean publish(String body, String topic);
}
