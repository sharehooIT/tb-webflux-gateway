package com.iot.gateway.model;

import lombok.Data;

@Data
public class BinaryProtocol {

    private String name;
}
