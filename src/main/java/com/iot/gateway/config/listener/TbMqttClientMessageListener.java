package com.iot.gateway.config.listener;


import lombok.extern.slf4j.Slf4j;
import net.dreamlu.iot.mqtt.codec.ByteBufferUtil;
import net.dreamlu.iot.mqtt.core.client.IMqttClientMessageListener;
import net.dreamlu.iot.mqtt.spring.client.MqttClientSubscribe;
import org.springframework.stereotype.Service;

import java.nio.ByteBuffer;

/**
 * 客户端消息监听的另一种方式
 *
 * @author L.cm
 */
@Slf4j
@Service
@MqttClientSubscribe("${topic1:/test/1}")
public class TbMqttClientMessageListener implements IMqttClientMessageListener {

    @Override
    public void onMessage(String topic, ByteBuffer payload) {
        log.info("topic:{} payload:{}", topic, ByteBufferUtil.toString(payload));
    }
}
