package com.iot.gateway.config.listener;

import lombok.extern.slf4j.Slf4j;
import net.dreamlu.iot.mqtt.core.client.IMqttClientConnectListener;
import org.springframework.stereotype.Service;
import org.tio.core.ChannelContext;

/**
 * 客户端连接状态监听
 *
 * @author L.cm
 */

@Slf4j
@Service
public class TbMqttClientConnectListener implements IMqttClientConnectListener {

    @Override
    public void onConnected(ChannelContext context, boolean isReconnect) {
        if (isReconnect) {
            log.info("重连 mqtt 服务器重连成功...");
        } else {
            log.info("连接 mqtt 服务器成功...");
        }
    }

    @Override
    public void onDisconnect(ChannelContext channelContext, Throwable throwable, String remark, boolean isRemove) {
        log.error("mqtt 链接断开 remark:{} isRemove:{}", remark, isRemove, throwable);
    }
}
