package com.iot.gateway.repository;

import com.iot.gateway.model.Tweet;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import org.springframework.stereotype.Repository;

/**
 * @quthor Created by miki
 * @date on 22/04/04.
 */
@Repository
public interface TweetRepository extends ReactiveCassandraRepository<Tweet, Long> {

}
