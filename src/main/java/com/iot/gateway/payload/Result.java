package com.iot.gateway.payload;

import com.iot.gateway.payload.constant.Status;

import java.text.MessageFormat;


/**
 * @desc 结果集封装类
 * @author miki
 * @param <T>
 */
public class Result<T> {

    private Integer code;
    private String msg;

    private T data;

    public Result() {
    }


    public Result(Integer code, String msg){
        this.code = code;
        this.msg = msg;
    }


    public Result(T data) {
        this.code = 0;
        this.data = data;
    }


    private Result(Status status){
        if(null !=status){
            this.code = status.getCode();
            this.msg = status.getMsg();

        }
    }


    public static <T> Result<T> success(T data){
        return new Result<>(data);
    }

    public Boolean isSuccess(){
        return this.code == 0;
    }

    public static <T> Result<T> success(Status status){
        return new Result<>(status);
    }


    public static <T> Result<T> error(Status status){
        return new Result<>(status);
    }


    public static <T> Result<T> errorWithArgs(Status status, Object... args){
        return  new Result<>(status.getCode(), MessageFormat.format(status.getMsg(), args));
    }


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
